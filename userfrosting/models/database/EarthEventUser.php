<?php
        
namespace UserFrosting;

use \Illuminate\Database\Capsule\Manager as Capsule;

class EarthEventUser extends UFModel {

    protected static $_table_id = "earth_events_user";

    protected $_markers;


    /**
     * Create a new earth event object.
     *
     */
    public function __construct($properties = []) {
        parent::__construct($properties);
    }
    
    /**
     * Lazily load a collection of Users which belong to this group.
     */ 
    public function users(){
        $link_table = Database::getSchemaTable('earth_events')->name;
        return $this->belongsToMany('UserFrosting\User', $link_table);
    }
    
    public function save(array $options = []){
        // If this is being set as the default primary group, then any other group must be demoted to default group
        if ($this->is_default == GROUP_DEFAULT_PRIMARY){
            $current_default_primary = static::where('is_default', GROUP_DEFAULT_PRIMARY);
            // Exclude this object, if it exists in DB
            if ($this->id)
                $current_default_primary = $current_default_primary->where('id', '!=', $this->id);
            $current_default_primary->update(['is_default' => GROUP_DEFAULT]);
        }
        
        return parent::save($options);
    }
    
    /**
     * Delete this group from the database, along with any linked user and authorization rules
     *
     */
    public function delete(){        
        // Remove all user associations
        $this->users()->detach();
        
        // Remove all group auth rules
        $auth_table = Database::getSchemaTable('authorize_group')->name;
        Capsule::table($auth_table)->where("group_id", $this->id)->delete();
         
        // Reassign any primary users to the current default primary group
        $default_primary_group = Group::where('is_default', GROUP_DEFAULT_PRIMARY)->first();
        
        $user_table = Database::getSchemaTable('user')->name;
        Capsule::table($user_table)->where('primary_group_id', $this->id)->update(["primary_group_id" => $default_primary_group->id]);
        
        // TODO: assign user to the default primary group as well?
        
        // Delete the group        
        $result = parent::delete();        
        
        return $result;
    }  


     /**
     * Get an array of group_ids to which this User currently belongs, as currently represented in this object.
     *
     * This method does NOT modify the database.
     * @return array[int] An array of group_ids to which this User belongs.
     */    
    public function getAOIIds(){
        // Fetch from database, if not set
            $link_table = Database::getSchemaTable('earth_events_user')->name;
            $result = Capsule::table($link_table)->select("earth_event_id")->where("user_id", $this->id)->get();
            
            $this->_aois = [];
            foreach ($result as $aoi){
                $this->_aois[] = $aoi['earth_event_id'];
            }      
                
        return $this->_aois;
    }

    /**
     * Add a group_id to the list of _groups to which this User belongs, checking that the Group exists and the User isn't already a member.
     *     
     * This method does NOT modify the database.  Call `store` to persist to database. 
     * @param int $group_id The id of the group to add the user to.
     * @throws Exception The specified group does not exist.
     * @return User this User object.
     */  
    public function addAOI($earth_event_id){
        $this->getAOIIds();
        
        // Return if user already in aoi
        if (in_array($earth_event_id, $this->_aois))
            return $this;
        
        // Next, check that the requested group actually exists
        if (!EarthEvent::find($earth_event_id))
            throw new \Exception("The specified event no. ($earth_event_id) does not exist.");
                
        // Ok, add to the list of groups
        $this->_aois[] = $earth_event_id;
        
        return $this;        
    } 
}