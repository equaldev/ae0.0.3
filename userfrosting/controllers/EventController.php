<?php

namespace UserFrosting;

/**
 * GroupController Class
 *
 * Controller class for /groups/* URLs.  Handles group-related activities, including listing groups, CRUD for groups, etc.
 *
 * @package UserFrosting
 * @author Alex Weissman
 * @link http://www.userfrosting.com/navigating/#structure
 */
class EventController extends \UserFrosting\BaseController {

    /**
     * Create a new GroupController object.
     *
     * @param UserFrosting $app The main UserFrosting app.
     */
    public function __construct($app){
        $this->_app = $app;
    }

    /**
     * Renders the group listing page.
     *
     * This page renders a table of user groups, with dropdown menus for modifying those groups.
     * This page requires authentication (and should generally be limited to admins or the root user).
     * Request type: GET
     * @todo implement interface to modify authorization hooks and permissions
     */
public function renderNews(){
        // Access-controlled page
        //if (!$this->_app->user->checkAccess('uri_groups')){
          //  $this->_app->notFound();
        //}

    /*$rss = new DOMDocument();
    $rss->load('https://watchers.news/category/editors-picks/feed/');
    $feed = array();
    foreach ($rss->getElementsByTagName('item') as $node) {
        $item = array ( 
            'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
            'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
            'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
            'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
            );
        array_push($feed, $item);
    }
    $limit = 1;
    for($x=0;$x<$limit;$x++) {
        $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
        $link = $feed[$x]['link'];
        $description = $feed[$x]['desc'];
        $date = date('l F d, Y', strtotime($feed[$x]['date']));
        echo '<p><strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
        echo '<small><em>Posted on '.$date.'</em></small></p>';
        echo '<p>'.$description.'</p>';
    }

            $this->_app->render('dashboard.twig');
*/

$html = "";
$url = "https://watchers.news/category/editors-picks/feed/";
$xml = simplexml_load_file($url);
for($i = 0; $i < 2; $i++){
    $title = $xml->channel->item[$i]->title;
    $link = $xml->channel->item[$i]->link;
    $description = $xml->channel->item[$i]->description;
    $pubDate = $xml->channel->item[$i]->pubDate;
        $news = new EarthEvent;
    $news->title = $title;
    $news->link = $link;
    $news->desc = $description;
    $news->pubDate = $pubDate;
   // $html .= "<a href='$link'><h3>$title</h3></a>";
    //$html .= "$description";
    //$html .= "<br />$pubDate<hr />";
    //$news = "";
//echo $html
    }
$this->_app->render('dashboard.twig', [
            "news" => $news
        ]);   

}

public function renderMap(){
        // Access-controlled page
        //if (!$this->_app->user->checkAccess('uri_groups')){
          //  $this->_app->notFound();
        //}
        $descId = $_GET['id'];
        $earth_events = EarthEvent::queryBuilder()->get($descId);

        $this->_app->render('users/swipe.twig', [
            "earth_events" => $events
        ]);
    }


    public function pageEvents(){
        // Access-controlled page
        //if (!$this->_app->user->checkAccess('uri_groups')){
          //  $this->_app->notFound();
        //}

        $earth_events = EarthEvent::queryBuilder()->get();

        $this->_app->render('groups/aoi.twig', [
            "earth_events" => $earth_events
        ]);
    }

    public function pageEventAuthorization($group_id) {
        // Access-controlled page
        if (!$this->_app->user->checkAccess('uri_authorization_settings')){
            $this->_app->notFound();
        }

        $group = Group::find($group_id);

        // Load all auth rules
        $rules = GroupAuth::where('group_id', $group_id)->get();

        $this->_app->render('config/authorization.twig', [
            "group" => $group,
            "rules" => $rules
        ]);

    }


    /**
     * Renders the form for creating a new group.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the form, which can be embedded in other pages.
     * The form can be rendered in "modal" (for popup) or "panel" mode, depending on the value of the GET parameter `render`
     * This page requires authentication (and should generally be limited to admins or the root user).
     * Request type: GET
     */
    public function CreateAOIform(){
        // Access-controlled resource
        //if (!$this->_app->user->checkAccess('create_group')){
        //    $this->_app->notFound();
       //}
        $user_id = $this->_app->user->id;
        $get = $this->_app->request->get();

        if (isset($get['render']))
            $render = $get['render'];
        else
            $render = "modal";

        // Set default values
        $data['location_lat'] = "0";
        // Set default title for new users
        $data['location_lng'] = "0";
        // Set default theme
        $data['location_name'] = "default";
        // Set default icon
        //$data['icon'] = "fa fa-user";
        // Set default landing page
        //$data['landing_page'] = "dashboard";
        $data['user_id'] = $user_id;
        $data['aoi_label_icon'] = "fa fa-user";
        // Create a dummy Group to prepopulate fields
        $aoi = new EarthEvent($data);

        if ($render == "modal")
            $template = "components/common/add-aoi-form.twig";
        else
            $template = "components/common/add-aoi-form.twig";

        // Determine authorized fields
       // $fields = ['location_lat', 'location_lng', 'landing_page', 'location_name'];
       // $show_fields = [];
       // $disabled_fields = [];
        //foreach ($fields as $field){
        //    if ($this->_app->user->checkAccess("update_group_setting", ["property" => $field]))
       //         $show_fields[] = $field;
       //     else
       //         $disabled_fields[] = $field;
       // }

        // Load validator rules
        $schema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/aoi-create.json");
        $this->_app->jsValidator->setSchema($schema);

        $this->_app->render($template, [
            "submit_button" => "Monitor Area",
            "form_action" => $this->_app->site->uri['public'] . "/map/aoi/",
            "buttons" => [
                "hidden" => [
                    "edit", "delete"
                ]
            ],
            "validators" => $this->_app->jsValidator->rules()
        ]);
    }
 public function CreateAOIformMap(){
        // Access-controlled resource
        //if (!$this->_app->user->checkAccess('create_group')){
        //    $this->_app->notFound();
       //}
        $user_id = $this->_app->user->id;
        $get = $this->_app->request->get();

        if (isset($get['render']))
            $render = $get['render'];
        else
            $render = "modal";

        // Set default values
        $data['location_lat'] = "0";
        // Set default title for new users
        $data['location_lng'] = "0";
        // Set default theme
        $data['location_name'] = "default";
        // Set default icon
        //$data['icon'] = "fa fa-user";
        // Set default landing page
        //$data['landing_page'] = "dashboard";
        $data['user_id'] = $user_id;
        $data['aoi_label_icon'] = "fa fa-user";
        // Create a dummy Group to prepopulate fields
        $aoi = new EarthEvent($data);

        if ($render == "modal")
            $template = "components/common/add-aoi-form2.twig";
        else
            $template = "components/common/add-aoi-form2.twig";

        // Determine authorized fields
       // $fields = ['location_lat', 'location_lng', 'landing_page', 'location_name'];
       // $show_fields = [];
       // $disabled_fields = [];
        //foreach ($fields as $field){
        //    if ($this->_app->user->checkAccess("update_group_setting", ["property" => $field]))
       //         $show_fields[] = $field;
       //     else
       //         $disabled_fields[] = $field;
       // }

        // Load validator rules
        $schema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/aoi-create.json");
        $this->_app->jsValidator->setSchema($schema);

        $this->_app->render($template, [
            "submit_button" => "Monitor Area",
            "form_action" => $this->_app->site->uri['public'] . "/map/aoi/",
            "buttons" => [
                "hidden" => [
                    "edit", "delete"
                ]
            ],
            "validators" => $this->_app->jsValidator->rules()
        ]);
    }

    /**
     * Renders the form for editing an existing group.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the form, which can be embedded in other pages.
     * The form can be rendered in "modal" (for popup) or "panel" mode, depending on the value of the GET parameter `render`.
     * Any fields that the user does not have permission to modify will be automatically disabled.
     * This page requires authentication (and should generally be limited to admins or the root user).
     * Request type: GET
     * @param int $group_id the id of the group to edit.
     */
    public function formGroupEdit($group_id){
        // Access-controlled resource
        if (!$this->_app->user->checkAccess('uri_groups')){
            $this->_app->notFound();
        }

        $get = $this->_app->request->get();

        if (isset($get['render']))
            $render = $get['render'];
        else
            $render = "modal";

        // Get the group to edit
        $group = Group::find($group_id);

        // Get a list of all themes
        $theme_list = $this->_app->site->getThemes();

        if ($render == "modal")
            $template = "components/common/group-info-modal.twig";
        else
            $template = "components/common/group-info-panel.twig";

        // Determine authorized fields
        $fields = ['name', 'new_user_title', 'landing_page', 'theme', 'is_default'];
        $show_fields = [];
        $disabled_fields = [];
        $hidden_fields = [];
        foreach ($fields as $field){
            if ($this->_app->user->checkAccess("update_group_setting", ["property" => $field]))
                $show_fields[] = $field;
            else if ($this->_app->user->checkAccess("view_group_setting", ["property" => $field]))
                $disabled_fields[] = $field;
            else
                $hidden_fields[] = $field;
        }

        // Load validator rules
        $schema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/group-update.json");
        $this->_app->jsValidator->setSchema($schema);

        $this->_app->render($template, [
            "box_id" => $get['box_id'],
            "box_title" => "Edit Group",
            "submit_button" => "Update group",
            "form_action" => $this->_app->site->uri['public'] . "/groups/g/$group_id",
            "group" => $group,
            "themes" => $theme_list,
            "fields" => [
                "disabled" => $disabled_fields,
                "hidden" => $hidden_fields
            ],
            "buttons" => [
                "hidden" => [
                    "edit", "delete"
                ]
            ],
            "validators" => $this->_app->jsValidator->rules()
        ]);
    }

    /**
     * Processes the request to create a new group.
     *
     * Processes the request from the group creation form, checking that:
     * 1. The group name is not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see formGroupCreate
     */
    public function createAOI(){
        $post = $this->_app->request->post();

        // DEBUG: view posted data
        error_log(print_r($post, true));

        // Load the request schema
        $requestSchema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/aoi-create.json");

        // Get the alert message stream
        $ms = $this->_app->alerts;
        // Access-controlled resource
        //if (!$this->_app->user->checkAccess('create_group')){
        //    $ms->addMessageTranslated("danger", "ACCESS_DENIED");
        //    $this->_app->halt(403);
       // }

        // Set up Fortress to process the request
        $rf = new \Fortress\HTTPRequestFortress($ms, $requestSchema, $post);

        // Sanitize data
        $rf->sanitize();

        // Validate, and halt on validation errors.
        $error = !$rf->validate(true);

        // Get the filtered data
        $data = $rf->data();

        // Remove csrf_token from object data
        $rf->removeFields(['csrf_token']);

        // Perform desired data transformations on required fields.
        $data['location_name'] = trim($data['location_name']);
        $data['location_lng'] = trim($data['location_lng']);
        $data['location_lat'] = trim($data['location_lat']);
        $data['observe_until'] = trim($data['observe_until']);
        $data['observe_from'] = trim($data['observe_from']);
        $data['aoi_label'] = trim($data['aoi_label']);
        $data['aoi_label_icon'] = trim($data['aoi_label_icon']);

    
        // Check if group name already exists
        //if (Group::where('name', $data['name'])->first()){
        //    $ms->addMessageTranslated("danger", "GROUP_NAME_IN_USE", $post);
        //    $error = true;
        //}

        // Halt on any validation errors
        //if ($error) {
        //    $this->_app->halt(400);
        //}

        // Set default values if not specified or not authorized
       /* if (!isset($data['theme']) || !$this->_app->user->checkAccess("update_group_setting", ["property" => "theme"]))
            $data['theme'] = "default";

        if (!isset($data['new_user_title']) || !$this->_app->user->checkAccess("update_group_setting", ["property" => "new_user_title"])) {
            // Set default title for new users
            $data['new_user_title'] = "New User";
        }

        if (!isset($data['landing_page']) || !$this->_app->user->checkAccess("update_group_setting", ["property" => "landing_page"])) {
            $data['landing_page'] = "dashboard";
        }

        if (!isset($data['icon']) || !$this->_app->user->checkAccess("update_group_setting", ["property" => "icon"])) {
            $data['icon'] = "fa fa-user";
        }

        if (!isset($data['is_default']) || !$this->_app->user->checkAccess("update_group_setting", ["property" => "is_default"])) {
            $data['is_default'] = "0";
        }
8*/   
        // Create the group
        $event = new EarthEvent($data);

        // Store new aoi to database
        $event->store();
        //get event id
        //$eEventID = EarthEventUser::max('id');
        //add to other table
        
        // $link_table = Database::getSchemaTable('earth_events_user')->name;
         //   $result = Capsule::table($link_table)->select("earth_event_id")->where("user_id", $this->id)->get();
    //$new_earth_event = new EarthEventUser([
     // "user_id" => $this->_app->user->id,
     // "earth_event_id" => $eEventID
    //]);
  //$new_earth_event->store();
        // Success message
        $ms->addMessageTranslated("success", "GROUP_CREATION_SUCCESSFUL", $data);
    }


public function createMarker(){
        $post = $this->_app->request->post();

        // DEBUG: view posted data
        error_log(print_r($post, true));

        // Load the request schema
        $requestSchema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/marker.json");

        // Get the alert message stream
        $ms = $this->_app->alerts;

        // Set up Fortress to process the request
        $rf = new \Fortress\HTTPRequestFortress($ms, $requestSchema, $post);

        // Sanitize data
        $rf->sanitize();

        // Validate, and halt on validation errors.
        $error = !$rf->validate(true);

        // Get the filtered data
        $data = $rf->data();

        // Remove csrf_token from object data
        $rf->removeFields(['csrf_token']);

        // Perform desired data transformations on required fields.
        $data['user_id'] = trim($data['user_id']);
        $data['description'] = trim($data['description']);
        $data['latitude'] = trim($data['latitude']);
        $data['longitude'] = trim($data['longitude']);
        $data['marker_id'] = trim($data['marker_id']);


        // Create the group
        $event = new EarthEventUser($data);

        error_log(print_r($event, true));

        // Store new aoi to database
        $event->store();
        //get event id
        //$eEventID = EarthEventUser::max('id');
        //add to other table
        
        // $link_table = Database::getSchemaTable('earth_events_user')->name;
         //   $result = Capsule::table($link_table)->select("earth_event_id")->where("user_id", $this->id)->get();
    //$new_earth_event = new EarthEventUser([
     // "user_id" => $this->_app->user->id,
     // "earth_event_id" => $eEventID
    //]);
  //$new_earth_event->store();
        // Success message
        $ms->addMessageTranslated("success", "GROUP_CREATION_SUCCESSFUL", $data);
    }
    /**
     * Processes the request to update an existing group's details.
     *
     * Processes the request from the group update form, checking that:
     * 1. The group name is not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @param int $group_id the id of the group to edit.
     * @see formGroupEdit
     */
    public function updateGroup($group_id){
        $post = $this->_app->request->post();

        // DEBUG: view posted data
        //error_log(print_r($post, true));

        // Load the request schema
        $requestSchema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/group-update.json");

        // Get the alert message stream
        $ms = $this->_app->alerts;

        // Get the target group
        $group = Group::find($group_id);

        // If desired, put route-level authorization check here

        // Remove csrf_token
        unset($post['csrf_token']);

        // Check authorization for submitted fields, if the value has been changed
        foreach ($post as $name => $value) {
            if ($group->attributeExists($name) && $post[$name] != $group->$name){
                // Check authorization
                if (!$this->_app->user->checkAccess('update_group_setting', ['group' => $group, 'property' => $name])){
                    $ms->addMessageTranslated("danger", "ACCESS_DENIED");
                    $this->_app->halt(403);
                }
            } else if (!$group->attributeExists($name)) {
                $ms->addMessageTranslated("danger", "NO_DATA");
                $this->_app->halt(400);
            }
        }

        // Check that name is not already in use
        if (isset($post['name']) && $post['name'] != $group->name && Group::where('name', $post['name'])->first()){
            $ms->addMessageTranslated("danger", "GROUP_NAME_IN_USE", $post);
            $this->_app->halt(400);
        }

        // TODO: validate landing page route, theme, icon?

        // Set up Fortress to process the request
        $rf = new \Fortress\HTTPRequestFortress($ms, $requestSchema, $post);

        // Sanitize
        $rf->sanitize();

        // Validate, and halt on validation errors.
        if (!$rf->validate()) {
            $this->_app->halt(400);
        }

        // Get the filtered data
        $data = $rf->data();

        // Update the group and generate success messages
        foreach ($data as $name => $value){
            if ($value != $group->$name){
                $group->$name = $value;
                // Add any custom success messages here
            }
        }

        $ms->addMessageTranslated("success", "GROUP_UPDATE", ["name" => $group->name]);
        $group->store();

    }

    /**
     * Processes the request to delete an existing group.
     *
     * Deletes the specified group, removing associations with any users and any group-specific authorization rules.
     * Before doing so, checks that:
     * 1. The group is deleteable (as specified in the `can_delete` column in the database);
     * 2. The group is not currently set as the default primary group;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @param int $group_id the id of the group to delete.
     */
    public function deleteGroup($group_id){
        $post = $this->_app->request->post();

        // Get the target group
        $group = Group::find($group_id);

        // Get the alert message stream
        $ms = $this->_app->alerts;

        // Check authorization
        if (!$this->_app->user->checkAccess('delete_group', ['group' => $group])){
            $ms->addMessageTranslated("danger", "ACCESS_DENIED");
            $this->_app->halt(403);
        }

        // Check that we are allowed to delete this group
        if ($group->can_delete == "0"){
            $ms->addMessageTranslated("danger", "CANNOT_DELETE_GROUP", ["name" => $group->name]);
            $this->_app->halt(403);
        }

        // Do not allow deletion if this group is currently set as the default primary group
        if ($group->is_default == GROUP_DEFAULT_PRIMARY){
            $ms->addMessageTranslated("danger", "GROUP_CANNOT_DELETE_DEFAULT_PRIMARY", ["name" => $group->name]);
            $this->_app->halt(403);
        }

        $ms->addMessageTranslated("success", "GROUP_DELETION_SUCCESSFUL", ["name" => $group->name]);
        $group->delete();       // TODO: implement Group function
        unset($group);
    }


  /*public function pageGroupTitles(){
           // Access-controlled resource
           if (!$this->_app->user->checkAccess('uri_group_titles')){
               $this->_app->notFound();
           }
            
           // Get a list of all groups
           $groups = Group::get();
           
           $this->_app->render('group-titles.twig', [
               "groups" => $groups     
           ]);   
        }
    */
        public function updateGroupTitles(){
           // Access-controlled resource
           if (!$this->_app->user->checkAccess('uri_group_titles')){
               $this->_app->notFound();
           }
           
           // Fetch the POSTed data
           $post = $this->_app->request->post();
           
           // Load the request schema
           $requestSchema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/group-titles.json");
           
           // Get the alert message stream
           $ms = $this->_app->alerts; 
        
           // Set up Fortress to process the request
           $rf = new \Fortress\HTTPRequestFortress($ms, $requestSchema, $post);                    
            
           // Sanitize
           $rf->sanitize();
            
           // Validate, and halt on validation errors.
           if (!$rf->validate()) {
               $this->_app->halt(400);
           }   
                  
           // Get the filtered data
           $data = $rf->data();
           
           // Load all users whose primary group matches the requested group
           $users = User::where('primary_group_id', $post['group_id'])->get();
           
           // Update title for these users
            foreach ($users as $user){
              $user->title = $post['title'];
              $user->save();
            }
            
            // Give us a nice success message
            $ms->addMessageTranslated("success", "Everyone's title has been updated to {{title}}!", $post);
            
        }
          public function pageGroupTitles(){
           // Access-controlled resource
           if (!$this->_app->user->checkAccess('uri_group_titles')){
               $this->_app->notFound();
           }
           
           // Get the validation rules for the form on this page
           $schema = new \Fortress\RequestSchema($this->_app->config('schema.path') . "/forms/group-titles.json");
           $this->_app->jsValidator->setSchema($schema); 
           
           // Get a list of all groups
           $groups = Group::get();
           
           $this->_app->render('group-titles.twig', [
               "groups" => $groups,
               "validators" => $this->_app->jsValidator->rules()   
           ]);   
        }
}
