-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 26, 2017 at 05:07 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frost`
--

-- --------------------------------------------------------

--
-- Table structure for table `earth_events`
--

CREATE TABLE `earth_events` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `observe_from` varchar(50) NOT NULL,
  `observe_until` varchar(50) NOT NULL,
  `location_lat` varchar(50) NOT NULL,
  `location_lng` varchar(50) NOT NULL,
  `location_name` varchar(50) NOT NULL,
  `aoi_label` varchar(10) NOT NULL,
  `aoi_label_icon` varchar(30) NOT NULL,
  `location_description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earth_events`
--

INSERT INTO `earth_events` (`id`, `user_id`, `observe_from`, `observe_until`, `location_lat`, `location_lng`, `location_name`, `aoi_label`, `aoi_label_icon`, `location_description`, `created_at`, `modified`) VALUES
(6, 1, '2015-12-31T12:59:59.999Z', '2016-04-01T12:59:59.999Z', '150.867778', '-34.598611', 'Killalea State Park', 'Vegetation', 'fa fa-tree', '', '2016-10-23 05:07:32', '2016-11-06 05:17:06'),
(7, 1, '2014-3-31T12:59:59.999Z', '2015-03-31T12:59:59.999Z', '-155.286762', '19.421097', 'Kilauea Volcano', 'Volcano', 'wi wi-volcano', 'Kilauea is a currently active shield volcano in the Hawaiian Islands, and the most active of the five volcanoes that together form the island of Hawaii. Located along the southern shore of the island, the volcano is between 300,000 and 600,000 years old and emerged above sea level about 100,000 years ago. It is the second youngest product of the Hawaiian hotspot and the current eruptive center of the Hawaiian–Emperor seamount chain. Because it lacks topographic prominence and its activities historically coincided with those of Mauna Loa, Kilauea was once thought to be a satellite of its much larger neighbor. Structurally, Kilauea has a large, fairly recently formed caldera at its summit and two active rift zones, one extending 125 km (78 mi) east and the other 35 km (22 mi) west, as an active fault of unknown depth moving vertically an average of 2 to 20 mm (0.1 to 0.8 in) per year.', '2016-10-23 05:24:33', '2016-11-06 05:16:52'),
(9, 1, '2016-04-01T12:59:59.999Z', '2016-10-12T12:59:59.999Z', '161.361667', '56.653333', 'Shiveluch Volcano', 'Volcano', 'wi wi-volcano', 'Shiveluch began forming about 60,000 to 70,000 years ago, and it has had at least 60 large eruptions during the Holocene. During this era, the most intense period of volcanism — including frequent large and moderate eruptions — occurred around 6500–6400 BC, 2250–2000 BC, and AD 50–650. This coincides with the peak of activity in other Kamchatka volcanoes. The current active period started around 900 BC. Since then, the large and moderate eruptions has been following each other in 50 to 400 year-long intervals.Catastrophic eruptions took place in 1854 and 1956, when a large part of the lava dome collapsed and created a devastating debris avalanche.  The most recent eruption of Young Shiveluch started on August 15, 1999, and continues as of 2016. On February 27, 2015 Shiveluch erupted shooting ash into the atmosphere about 30,000 feet crossing the Bering sea and into Alaska.', '2016-10-23 06:42:52', '2016-11-06 05:16:49'),
(10, 4, '2015-06-21T12:59:59.999Z', '2016-06-21T12:59:59.999Z', '-43.467111', '-20.2065', 'Bento Rodrigues Dam Disaster', 'Flood', 'wi wi-tsunami', 'The Bento Rodrigues dam disaster occurred on 5 November 2015, when an iron ore tailings dam in Bento Rodrigues, a subdistrict of Mariana, Brazil, suffered a catastrophic failure,[4] causing flooding and at least 17 deaths.[3] At least 16 people have been injured.[1]  About 60 million cubic meters of iron waste flowed into the Doce River. Toxic brown mudflows reached the Atlantic Ocean 17 days later.[5] The total impact and environmental consequences to the river and the beaches near its mouth, or to the wildlife are still unclear.[6] This incident has been described as the worst environmental disaster in Brazil\'s history.[7][8][9]  The dam is a property of Samarco, a joint venture between Vale and BHP Billiton. Initially it was speculated that the causes for the collapse would be some weaknesses in the dam\'s structure that were described in a report of 2013 from the Brazilian authorities. BHP Billiton denies this version,[10] and the causes of the incident are still being investigated.', '2016-10-23 10:34:50', '2016-11-06 05:16:46'),
(13, 3, '2015-06-21T12:59:59.999Z', '2016-06-21T12:59:59.999Z', '52.836632', '-169.850693', 'Cleveland Volcano', 'Volcano', 'wi wi-volcano', 'Beautifully symmetrical Mount Cleveland stratovolcano is situated at the western end of the uninhabited, dumbbell-shaped Chuginadak Island. It lies SE across Carlisle Pass strait from Carlisle volcano and NE across Chuginadak Pass strait from Herbert volcano.', '2016-10-26 10:46:05', '2016-11-06 05:16:29'),
(14, 1, '2014-3-31T12:59:59.999Z', '2016-09-31T12:59:59.999Z', '43.13', '36.34', 'Mosul', 'War', 'fa fa-bomb', 'Mosul, Iraq. Watchin\' the world change.', '2016-10-28 11:00:18', '2016-11-06 05:16:15'),
(16, 1, '2015-04-01T12:59:59.999Z', '2016-04-01T12:59:59.999Z', '137.366667', '-28.366667', 'Lake Eyre', 'Flood', 'wi wi-flood', 'Lake Eyre in South Australia, again.', '2016-10-29 00:13:41', '2016-11-06 05:16:06'),
(31, 1, '2016-10-21T12:59:59.999Z', '2016-10-31T12:59:59.999Z', '13.133333', '42.95', 'Ussita, Italy', 'Earthquake', 'wi wi-earthquake', 'A strong and shallow earthquake registered by the EMSC as M6.5 hit central Italy at 06:40 UTC on October 30, 2016. The agency is reporting a depth of 10 km (6.2 miles). USGS is reporting M6.6 at a depth of 10 km.\r\n\r\nAccording to the EMSC, the epicenter was located 16 km (9.9 miles) N of Maltignano (population 2 600), 38 km (23.6 miles) W of Ascoli Piceno (population 51 400), 59 km (36.6 miles) NW of L’Aquila (population 68 600) and 117 km (72.7 miles) NE of Roma (population 2 564 000), Italy.\r\n\r\nThere are 2 744 538 people living within 100 km (62 miles). Some 30 million people are estimated to be living in the felt area.\r\n\r\nThe quake comes just 4 days after M6.1 hit the same region and two months after deadly M6.2 of August 24, 2016. \r\n\r\nEMSC said this is the strongest earthquake in Italy since 1980, and an unprecedented sequence of 3 large shocks in 4 days for the country - M5.5, M6.1 and M6.5 - the strongest ground motion ever recorded.\r\n\r\nThere were no immediate reports of casualties, but tremors were felt in the capital Rome. This region is mostly evacuated since earthquakes started in August.\r\n\r\nThe mayor of the village of Ussita told Ansa news agency: \"Everything collapsed. I can see columns of smoke, it\'s a disaster. I was sleeping in the car and I saw hell.\"\r\n\r\nServices on the metro in Rome have been suspended since the quake.\r\n\r\nNumerous buildings near the epicenter have reportedly collapsed.', '2016-10-31 11:50:04', '2016-11-06 05:15:08'),
(32, 1, '2016-10-01T12:59:59.999Z', '2016-11-4T12:59:59.999Z', '150.824444', '-34.428611', 'Mount Kembla Backburning', 'Bushfire', 'wi wi-fire', 'Backburning on the Western slope of Mt Kembla.', '2016-11-04 23:45:20', '2016-11-06 05:16:00'),
(33, 1, '2013-12-31T13:00:00.000Z', '2016-11-15T00:00:00.000Z', '148.120273', '-30.0306887', 'Illegal Land clearing at Walgett, Northern NSW.', 'Vegetation', 'fa fa-tree', 'In 2015, a study by NSW Parks and Wildlife found that 60,000 hectares was being cleared per year in the state — a four-fold increase on previous State Government figures.\r\n\r\nSince losing their properties to the bank, the Priestleys say they have watched with growing anger and disbelief as native vegetation has been cleared — they claim without permission — to expand wheat fields.\r\n\r\nFor two years until end of 2015, the Priestleys collected evidence of alleged illegal clearing and sent it to the State Government watchdog, the Office of Environment and Heritage (OEH).\r\n\r\n Machinery sits in field in NSW\r\nPHOTO: Machinery sits in a field that the Priestleys say was cleared of native vegetation without permission. (ABC: Supplied)\r\nLast year, without explanation, the OEH halted its investigation.\r\n\r\n\"I\'m completely devastated,\" Claire Priestley said.\r\n\r\n\"I\'ve grown up on that land, I\'ve been out there my whole life. It\'s devastating to see that a large conglomerate can come into this community, it seems like they have a special privilege to just clear what they want.\"\r\n\r\nMr Priestley has photographed what he claims is the aftermath of broadscale land clearing, including images of several bulldozers and piles of smouldering native vegetation.\r\n\r\n\"You can virtually clear the size of the moon and get away with it but you can be in trouble for trespass by taking photos.\"\r\n\r\nApprovals for native clearing are required to be on a public register.\r\n\r\nThe NSW Environmental Defenders Office recently searched for any approvals given to the Harris business to clear land on its properties west of Walgett.\r\n\r\n\"We have searched those registers. They\'re complex registers. You navigate through them by GPS coordinates,\" EDO chief solicitor Sue Higginson told Lateline.', '2016-11-14 22:22:57', '0000-00-00 00:00:00'),
(34, 1, '', '', 'w', 'w', 'testing', 'Volcano', '', 'd', '2016-12-09 05:23:25', '0000-00-00 00:00:00'),
(35, 1, '2016-12-08T13:00:00.000Z', '2016-12-09T00:00:00.000Z', 's', 'sss', 's', 'Volcano', '', 'sdf;knsdkf', '2016-12-09 05:24:12', '0000-00-00 00:00:00'),
(36, 1, '2015-06-09T14:00:00.000Z', '2016-12-09T00:00:00.000Z', '51.47812216586772', '-0.1768970489501953', 'testing', 'Earthquake', 'wi wi-earthquake', 'testingngnignignginging', '2016-12-09 05:29:05', '0000-00-00 00:00:00'),
(37, 1, '2015-07-14T14:00:00.000Z', '2016-12-09T00:00:00.000Z', '-33.8628789197003', '151.20152413845062', 'Barangaroo', 'Flood', 'wi wi-flood', 'Barangoo', '2016-12-09 05:35:38', '0000-00-00 00:00:00'),
(38, 1, '2016-08-18T14:00:00.000Z', '2016-12-09T00:00:00.000Z', '-34.22656448721113', '151.52034759521484', 'Wollongong', 'War', 'fa fa-bomb', 'skd', '2016-12-09 05:42:42', '0000-00-00 00:00:00'),
(39, 1, '2014-06-09T14:00:00.000Z', '2016-12-09T00:00:00.000Z', '150.59195995330813', '-34.90448089498263', 'Nowra', 'Vegetation', 'fa fa-tree', 'kl/l', '2016-12-09 05:44:56', '0000-00-00 00:00:00'),
(40, 1, '2014-12-31T13:00:00.000Z', '2016-12-09T00:00:00.000Z', '150.47078847885135', '-35.12778872357724', 'Wandandian', 'Vegetation', 'fa fa-tree', 'fff', '2016-12-09 05:52:42', '0000-00-00 00:00:00'),
(41, 1, '2013-12-31T13:00:00.000Z', '2016-12-16T00:00:00.000Z', '37.22541332244874', '36.17806820921328', 'Aleppo', 'War', 'fa fa-bomb', 'war mate.', '2016-12-16 10:26:45', '0000-00-00 00:00:00'),
(42, 1, '2016-06-13T14:00:00.000Z', '2016-12-16T00:00:00.000Z', '98.28272581100465', '19.577440712064156', 'cavelodge', 'Vegetation', 'fa fa-tree', 'km', '2016-12-16 10:35:23', '0000-00-00 00:00:00'),
(43, 1, '2014-06-16T14:00:00.000Z', '2016-12-31T00:00:00.000Z', '151.50970458984378', '-34.2424594873685', 'Wollongong', 'Mining/Dev', 'fa fa-building', 'blah lbha', '2016-12-31 11:46:32', '0000-00-00 00:00:00'),
(45, 4, '2015-12-19T13:00:00.000Z', '2016-06-12T00:00:00.000Z', '143.8894844055176', '-38.63457282385874', 'Great Southern Road fire, Victoria', 'Bushfire', 'wi wi-fire', 'Updated coordinates for previous entry.', '2017-01-14 23:58:25', '2017-01-15 00:03:16'),
(46, 4, '2016-03-30T13:00:00.000Z', '2016-11-24T00:00:00.000Z', '150.7969665527344', '-35.043770597815225', 'Honeymoon Bay', 'Bushfire', 'wi wi-fire', 'Honeymoon Bay fire', '2017-01-15 00:05:59', '0000-00-00 00:00:00'),
(47, 4, '2014-02-28T13:00:00.000Z', '2017-03-05T00:00:00.000Z', '43.139877319335945', '36.31623169903713', 'Mosul, Iraq', 'War', 'fa fa-bomb', 'boom', '2017-03-04 23:04:34', '0000-00-00 00:00:00'),
(48, 1, '2014-06-02T14:00:00.000Z', '2017-03-05T00:00:00.000Z', '-121.49200916290285', '39.542837847189524', 'Oroville Dam Break', 'Volcano', 'wi wi-flood', 'Oroville Dam was overfilled and the emergency spillway absolutely destroyed the mountain side.', '2017-03-04 23:21:02', '2017-03-04 23:22:33'),
(49, 1, '2013-12-31T13:00:00.000Z', '2017-03-01T00:00:00.000Z', '20.067°E', '23.050°N', 'Tsengwen', 'Volcano', '', 'dam\r\n', '2017-03-23 23:31:28', '0000-00-00 00:00:00'),
(50, 1, '2013-12-31T13:00:00.000Z', '2017-03-24T00:00:00.000Z', '120.07618904113771', '23.0587265443298', 'Tsengwen Dam', 'Flood', 'wi wi-flood', 'hfh', '2017-03-23 23:54:57', '0000-00-00 00:00:00'),
(51, 1, '2016-11-30T13:00:00.000Z', '2017-03-28T00:00:00.000Z', '-122.51953125000001', '37.96477144899956', 'test', 'Erosion', 'wi wi-tsunami', 'v,lj', '2017-03-28 05:34:23', '0000-00-00 00:00:00'),
(52, 1, '2016-11-30T13:00:00.000Z', '2017-03-28T00:00:00.000Z', '149.19982910156253', '-21.225381663722256', 'das', 'Bushfire', 'wi wi-fire', 'sfdsdfsd', '2017-03-28 05:42:52', '0000-00-00 00:00:00'),
(53, 1, '2014-06-09T14:00:00.000Z', '2017-04-30T00:00:00.000Z', '-113.1207275390625', '48.90083790234091', 'Glacier Test', 'Flood', 'wi wi-flood', 'observing glacial melt', '2017-04-30 00:36:35', '0000-00-00 00:00:00'),
(54, 1, '2016-06-29T14:00:00.000Z', '2017-04-30T00:00:00.000Z', '-114.08509433269501', '48.87979158428683', 'Rainbow Glacier', 'Flood', 'wi wi-flood', 'Glacier National Park', '2017-04-30 00:56:12', '0000-00-00 00:00:00'),
(55, 4, '2016-03-31T13:00:00.000Z', '2017-04-30T00:00:00.000Z', '-113.73106956481934', '48.75392530642959', 'Grinnell Glacier', 'Volcano', '', 'g', '2017-04-30 01:48:35', '0000-00-00 00:00:00'),
(56, 1, '2014-06-09T14:00:00.000Z', '2017-09-19T00:00:00.000Z', '-70.40039062500001', '27.664068965384516', 'Hurricane Irma', 'Flood', 'wi wi-flood', 'Destruction relating to wind, tidal surge and infrastructure failure.', '2017-09-19 01:37:32', '0000-00-00 00:00:00'),
(57, 1, '2015-03-03T13:00:00.000Z', '2017-09-19T00:00:00.000Z', '-81.19445800781251', '25.549875683500645', 'Hurricane Irma - Tampa', 'Flood', 'wi wi-flood', 'd', '2017-09-19 01:42:38', '0000-00-00 00:00:00'),
(58, 1, '2017-05-31T14:00:00.000Z', '2017-09-19T00:00:00.000Z', '-97.108154296875', '28.13012773787403', 'Hurricane Harvey', 'Flood', 'wi wi-flood', 'Observing HurricaneHarvey make landfall in Rockport, Texas', '2017-09-19 03:46:54', '0000-00-00 00:00:00'),
(59, 1, '2017-08-23T14:00:00.000Z', '2017-08-25T00:00:00.000Z', '-94.99877929687501', '29.596147812456916', 'Houston Flooding', 'Flood', 'wi wi-flood', 'Flooding', '2017-09-19 06:35:55', '0000-00-00 00:00:00'),
(60, 1, '2017-07-31T14:00:00.000Z', '2017-09-08T00:00:00.000Z', '-94.93560791015626', '29.489815619374962', 'Houston Again', 'Volcano', '', 'kgcjg', '2017-09-19 06:38:49', '0000-00-00 00:00:00'),
(61, 1, '2017-07-11T14:00:00.000Z', '2017-09-19T00:00:00.000Z', '-77.34649658203126', '25.06072125231416', 'Bahamas', 'Flood', 'wi wi-flood', 'Island sediments\r\n', '2017-09-19 08:47:42', '0000-00-00 00:00:00'),
(62, 1, '2017-07-31T14:00:00.000Z', '2017-09-22T00:00:00.000Z', '-94.9658203125', '29.248063243796576', 'Hurricane Harvey', 'Flood', 'wi wi-flood', 'aETB;ksjBG', '2017-09-22 02:43:38', '0000-00-00 00:00:00'),
(63, 1, '2017-09-06T14:00:00.000Z', '2017-09-23T00:00:00.000Z', '-72.02636718750001', '24.726874870506972', 'Latest Maria image', 'Flood', 'wi wi-flood', 'jh', '2017-09-23 09:45:11', '0000-00-00 00:00:00'),
(64, 1, '2017-05-31T14:00:00.000Z', '2017-09-24T00:00:00.000Z', '-122.01965332031251', '40.63896734381725', 'Calidornia Wildfires', 'Bushfire', 'wi wi-fire', 'Tracing damage from wildfires.', '2017-09-24 11:40:14', '0000-00-00 00:00:00'),
(65, 1, '2017-09-05T14:00:00.000Z', '2017-09-23T00:00:00.000Z', '-81.05712890625001', '24.751819904924858', 'Florida Keys', 'Vegetation', 'fa fa-tree', 'Centered on Florida Keys to observe damage from Hurricane Irma >= 23 September 2017.', '2017-09-28 03:41:35', '0000-00-00 00:00:00'),
(66, 1, '2017-09-05T14:00:00.000Z', '2017-09-23T00:00:00.000Z', '-81.06811523437501', '24.739348013248694', 'Hurricane Irma', 'Flood', 'wi wi-flood', 'ihgvgvglvj', '2017-09-28 08:37:30', '0000-00-00 00:00:00'),
(67, 1, '2017-08-31T14:00:00.000Z', '2017-09-23T00:00:00.000Z', '-98.46513748168947', '18.600696763689527', 'Mexico Earthquake Izúcar de Matamoros', 'Earthquake', 'wi wi-earthquake', 'Observing infrastructure damage in Puebla Region of Mexico from magnitude 7.1 earthquake that struck on ~19th September 2017.', '2017-10-04 00:45:32', '0000-00-00 00:00:00'),
(68, 1, '2017-08-31T14:00:00.000Z', '2017-10-04T00:00:00.000Z', '-66.11297607421876', '18.432713391700858', 'Puerto Rico Damage from Irma/Maria', 'Flood', 'wi wi-flood', 'Tracking hurricane and flooding damage from Hurricane Maria/Irma.', '2017-10-04 04:15:30', '0000-00-00 00:00:00'),
(69, 1, '2017-07-31T14:00:00.000Z', '2017-10-04T00:00:00.000Z', '-81.78359985351564', '26.14218603473982', 'Naples, Florida', 'Flood', 'wi wi-flood', 'Tracking infrastructure damage from Hurricane Irma on the West Coast of Florida.', '2017-10-04 04:25:39', '0000-00-00 00:00:00'),
(70, 1, '2017-09-30T13:00:00.000Z', '2017-10-13T00:00:00.000Z', '-122.28607177734376', '38.29586467286608', 'California Wildfires', 'Bushfire', 'wi wi-fire', 'Observing damage from the 2017 wildfire season in California. Current fire impacting Napa Valley', '2017-10-13 02:58:03', '0000-00-00 00:00:00'),
(71, 1, '2017-09-30T13:00:00.000Z', '2017-10-13T00:00:00.000Z', '-104.54589843750001', '43.13306116240615', 'Napa', 'Bushfire', 'wi wi-fire', 'FIRE.', '2017-10-18 23:08:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `earth_events_user`
--

CREATE TABLE `earth_events_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `earth_event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earth_events_user`
--

INSERT INTO `earth_events_user` (`id`, `user_id`, `earth_event_id`) VALUES
(1, 1, 7),
(2, 3, 9),
(3, 1, 13),
(4, 1, 0),
(5, 1, 0),
(6, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `markers`
--

CREATE TABLE `markers` (
  `marker_id` int(10) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uf_authorize_group`
--

CREATE TABLE `uf_authorize_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `hook` varchar(200) NOT NULL COMMENT 'A code that references a specific action or URI that the group has access to.',
  `conditions` text NOT NULL COMMENT 'The conditions under which members of this group have access to this hook.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uf_authorize_group`
--

INSERT INTO `uf_authorize_group` (`id`, `group_id`, `hook`, `conditions`) VALUES
(1, 1, 'uri_dashboard', 'always()'),
(2, 2, 'uri_dashboard', 'always()'),
(3, 2, 'uri_users', 'always()'),
(4, 1, 'uri_account_settings', 'always()'),
(5, 1, 'update_account_setting', 'equals(self.id, user.id)&&in(property,[\"email\",\"locale\",\"password\"])'),
(6, 2, 'update_account_setting', '!in_group(user.id,2)&&in(property,[\"email\",\"display_name\",\"title\",\"locale\",\"flag_password_reset\",\"flag_enabled\"])'),
(7, 2, 'view_account_setting', 'in(property,[\"user_name\",\"email\",\"display_name\",\"title\",\"locale\",\"flag_enabled\",\"groups\",\"primary_group_id\"])'),
(8, 2, 'delete_account', '!in_group(user.id,2)'),
(9, 2, 'create_account', 'always()'),
(10, 1, 'uri_account-groups', 'always()'),
(15, 2, 'uri-map-swipe', 'always()'),
(16, 2, 'uri-map-layers', 'always()'),
(17, 4, 'uri_map_swipe', 'always()'),
(18, 4, 'uri_map_layers', 'always()'),
(19, 3, 'uri_map_swipe', 'always()');

-- --------------------------------------------------------

--
-- Table structure for table `uf_authorize_user`
--

CREATE TABLE `uf_authorize_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `hook` varchar(200) NOT NULL COMMENT 'A code that references a specific action or URI that the user has access to.',
  `conditions` text NOT NULL COMMENT 'The conditions under which the user has access to this action.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uf_configuration`
--

CREATE TABLE `uf_configuration` (
  `id` int(10) UNSIGNED NOT NULL,
  `plugin` varchar(50) NOT NULL COMMENT 'The name of the plugin that manages this setting (set to ''userfrosting'' for core settings)',
  `name` varchar(150) NOT NULL COMMENT 'The name of the setting.',
  `value` longtext NOT NULL COMMENT 'The current value of the setting.',
  `description` text NOT NULL COMMENT 'A brief description of this setting.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A configuration table, mapping global configuration options to their values.';

--
-- Dumping data for table `uf_configuration`
--

INSERT INTO `uf_configuration` (`id`, `plugin`, `name`, `value`, `description`) VALUES
(1, 'userfrosting', 'site_title', 'After Earth', 'The title of the site.  By default, displayed in the title tag, as well as the upper left corner of every user page.'),
(2, 'userfrosting', 'admin_email', 'developer@eqd.com.au', 'The administrative email for the site.  Automated emails, such as verification emails and password reset links, will come from this address.'),
(3, 'userfrosting', 'email_login', '1', 'Specify whether users can login via email address or username instead of just username.'),
(4, 'userfrosting', 'can_register', '1', 'Specify whether public registration of new accounts is enabled.  Enable if you have a service that users can sign up for, disable if you only want accounts to be created by you or an admin.'),
(5, 'userfrosting', 'enable_captcha', '1', 'Specify whether new users must complete a captcha code when registering for an account.'),
(6, 'userfrosting', 'require_activation', '1', 'Specify whether email verification is required for newly registered accounts.  Accounts created by another user never need to be verified.'),
(7, 'userfrosting', 'resend_activation_threshold', '0', 'The time, in seconds, that a user must wait before requesting that the account verification email be resent.'),
(8, 'userfrosting', 'reset_password_timeout', '10800', 'The time, in seconds, before a user\'s password reset token expires.'),
(9, 'userfrosting', 'create_password_expiration', '86400', 'The time, in seconds, before a new user\'s password creation token expires.'),
(10, 'userfrosting', 'default_locale', 'en_US', 'The default language for newly registered users.'),
(11, 'userfrosting', 'guest_theme', 'default', 'The template theme to use for unauthenticated (guest) users.'),
(12, 'userfrosting', 'minify_css', '0', 'Specify whether to use concatenated, minified CSS (production) or raw CSS includes (dev).'),
(13, 'userfrosting', 'minify_js', '0', 'Specify whether to use concatenated, minified JS (production) or raw JS includes (dev).'),
(14, 'userfrosting', 'version', '0.3.1.20', 'The current version of UserFrosting.'),
(15, 'userfrosting', 'author', 'Brenna Bensley', 'The author of the site.  Will be used in the site\'s author meta tag.'),
(16, 'userfrosting', 'show_terms_on_register', '1', 'Specify whether or not to show terms and conditions when registering.'),
(17, 'userfrosting', 'site_location', 'Australia', 'The nation or state in which legal jurisdiction for this site falls.'),
(18, 'userfrosting', 'install_status', 'complete', ''),
(19, 'userfrosting', 'root_account_config_token', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `uf_group`
--

CREATE TABLE `uf_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Specifies whether this permission is a default setting for new accounts.',
  `can_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Specifies whether this permission can be deleted from the control panel.',
  `theme` varchar(100) NOT NULL DEFAULT 'default' COMMENT 'The theme assigned to primary users in this group.',
  `landing_page` varchar(200) NOT NULL DEFAULT 'dashboard' COMMENT 'The page to take primary members to when they first log in.',
  `new_user_title` varchar(200) NOT NULL DEFAULT 'New User' COMMENT 'The default title to assign to new primary users.',
  `icon` varchar(100) NOT NULL DEFAULT 'fa fa-user' COMMENT 'The icon representing primary users in this group.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uf_group`
--

INSERT INTO `uf_group` (`id`, `name`, `is_default`, `can_delete`, `theme`, `landing_page`, `new_user_title`, `icon`) VALUES
(1, 'User', 2, 0, 'default', 'dashboard', 'New User', 'fa fa-user'),
(2, 'Administrator', 0, 0, 'root', 'dashboard', 'Brood Spawn', 'fa fa-flag'),
(3, 'Student', 1, 1, 'nyx', 'dashboard', 'FNG', 'sc sc-zergling'),
(4, 'Enterprise', 0, 1, 'nyx', 'dashboard', 'Space Cadet', 'fa fa-user'),
(5, 'testtt', 0, 1, 'default', 'dashboard', 'New User', 'fa fa-user');

-- --------------------------------------------------------

--
-- Table structure for table `uf_group_user`
--

CREATE TABLE `uf_group_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps users to their group(s)';

--
-- Dumping data for table `uf_group_user`
--

INSERT INTO `uf_group_user` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(4, 3, 1),
(7, 3, 4),
(8, 4, 1),
(9, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `uf_user`
--

CREATE TABLE `uf_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `locale` varchar(10) NOT NULL DEFAULT 'en_US' COMMENT 'The language and locale to use for this user.',
  `primary_group_id` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'The id of this user''s primary group.',
  `secret_token` varchar(32) NOT NULL DEFAULT '' COMMENT 'The current one-time use token for various user activities confirmed via email.',
  `flag_verified` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Set to ''1'' if the user has verified their account via email, ''0'' otherwise.',
  `flag_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Set to ''1'' if the user''s account is currently enabled, ''0'' otherwise.  Disabled accounts cannot be logged in to, but they retain all of their data and settings.',
  `flag_password_reset` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Set to ''1'' if the user has an outstanding password reset request, ''0'' otherwise.',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uf_user`
--

INSERT INTO `uf_user` (`id`, `user_name`, `display_name`, `email`, `title`, `locale`, `primary_group_id`, `secret_token`, `flag_verified`, `flag_enabled`, `flag_password_reset`, `created_at`, `updated_at`, `password`) VALUES
(1, 'dev', 'Dev', 'brenna.bensley@live.com', 'New User', 'en_US', 1, '', 1, 1, 0, '2016-10-16 10:51:44', '2016-10-16 10:51:44', '$2y$10$0/3hrGqXsYJN7VYL0OkufOK19kqJxyvWcVuLmNGxJi0gfeKBtWtCO'),
(3, 'test', 'Enterprise', 'test@test.com', 'New User', 'en_US', 1, '9148ab708bea2fd3d91b2aabceb86f7d', 1, 1, 0, '2016-10-21 10:54:15', '2016-10-29 13:38:14', '$2y$10$pW6SLCyYza1dN1DyrhoQIuZ6H06fKdLU3xI9YBXQJPP3P5qFWloDK'),
(4, 'Student', 'Student', 'stu@dent.com', 'New User', 'en_US', 1, 'd3c3cdde4b1b41ed1b510b678318d60c', 1, 1, 0, '2016-10-21 11:24:31', '2016-10-29 13:37:50', '$2y$10$StnuDK5CArrb2BwvMGmKDuYRaIR7CIe6g0Qd8gksaldfuNmkv6uma');

-- --------------------------------------------------------

--
-- Table structure for table `uf_user_event`
--

CREATE TABLE `uf_user_event` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `event_type` varchar(255) NOT NULL COMMENT 'An identifier used to track the type of event.',
  `occurred_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uf_user_event`
--

INSERT INTO `uf_user_event` (`id`, `user_id`, `event_type`, `occurred_at`, `description`) VALUES
(1, 1, 'sign_up', '2016-10-17 01:51:45', 'User dev successfully registered on 2016-10-16 21:51:44.'),
(2, 1, 'sign_in', '2016-10-17 01:52:00', 'User dev signed in at 2016-10-16 21:52:00.'),
(5, 1, 'sign_in', '2016-10-17 02:19:46', 'User dev signed in at 2016-10-16 22:19:46.'),
(6, 1, 'sign_in', '2016-10-17 02:24:14', 'User dev signed in at 2016-10-16 22:24:14.'),
(7, 1, 'sign_in', '2016-10-17 02:30:59', 'User dev signed in at 2016-10-16 22:30:59.'),
(8, 1, 'sign_in', '2016-10-17 03:56:01', 'User dev signed in at 2016-10-16 23:56:01.'),
(9, 1, 'sign_in', '2016-10-18 05:12:54', 'User dev signed in at 2016-10-18 01:12:54.'),
(10, 1, 'sign_in', '2016-10-18 05:12:58', 'User dev signed in at 2016-10-18 01:12:58.'),
(11, 1, 'sign_in', '2016-10-18 07:31:10', 'User dev signed in at 2016-10-18 03:31:10.'),
(12, 1, 'sign_in', '2016-10-19 23:51:27', 'User dev signed in at 2016-10-19 19:51:26.'),
(14, 1, 'sign_in', '2016-10-20 00:47:54', 'User dev signed in at 2016-10-19 20:47:54.'),
(16, 1, 'sign_in', '2016-10-20 01:36:22', 'User dev signed in at 2016-10-19 21:36:22.'),
(17, 1, 'sign_in', '2016-10-20 07:24:35', 'User dev signed in at 2016-10-20 03:24:35.'),
(18, 1, 'sign_in', '2016-10-20 20:53:56', 'User dev signed in at 2016-10-20 16:53:56.'),
(21, 3, 'sign_up', '2016-10-22 01:54:16', 'User test successfully registered on 2016-10-21 21:54:15.'),
(22, 3, 'verification_request', '2016-10-22 01:54:16', 'User test requested verification on 2016-10-21 21:54:16.'),
(23, 1, 'sign_in', '2016-10-22 01:54:30', 'User dev signed in at 2016-10-21 21:54:30.'),
(24, 3, 'sign_in', '2016-10-22 01:55:21', 'User test signed in at 2016-10-21 21:55:21.'),
(25, 1, 'sign_in', '2016-10-22 01:55:43', 'User dev signed in at 2016-10-21 21:55:43.'),
(26, 1, 'sign_in', '2016-10-22 02:02:55', 'User dev signed in at 2016-10-21 22:02:55.'),
(27, 3, 'sign_in', '2016-10-22 02:04:57', 'User test signed in at 2016-10-21 22:04:57.'),
(29, 1, 'sign_in', '2016-10-22 02:08:32', 'User dev signed in at 2016-10-21 22:08:32.'),
(31, 3, 'sign_in', '2016-10-22 02:15:09', 'User test signed in at 2016-10-21 22:15:09.'),
(32, 1, 'sign_in', '2016-10-22 02:17:42', 'User dev signed in at 2016-10-21 22:17:42.'),
(33, 1, 'sign_in', '2016-10-22 02:17:45', 'User dev signed in at 2016-10-21 22:17:45.'),
(34, 3, 'sign_in', '2016-10-22 02:21:49', 'User test signed in at 2016-10-21 22:21:49.'),
(37, 4, 'sign_up', '2016-10-22 02:24:32', 'User Student successfully registered on 2016-10-21 22:24:31.'),
(38, 4, 'verification_request', '2016-10-22 02:24:32', 'User Student requested verification on 2016-10-21 22:24:32.'),
(39, 1, 'sign_in', '2016-10-22 02:24:40', 'User dev signed in at 2016-10-21 22:24:40.'),
(40, 4, 'sign_in', '2016-10-22 02:25:26', 'User Student signed in at 2016-10-21 22:25:26.'),
(41, 4, 'sign_in', '2016-10-22 02:25:51', 'User Student signed in at 2016-10-21 22:25:51.'),
(42, 3, 'sign_in', '2016-10-22 02:26:11', 'User test signed in at 2016-10-21 22:26:11.'),
(43, 1, 'sign_in', '2016-10-22 02:26:36', 'User dev signed in at 2016-10-21 22:26:36.'),
(44, 3, 'sign_in', '2016-10-22 11:29:16', 'User test signed in at 2016-10-22 07:29:16.'),
(45, 1, 'sign_in', '2016-10-22 11:31:01', 'User dev signed in at 2016-10-22 07:31:01.'),
(46, 1, 'sign_in', '2016-10-22 23:09:23', 'User dev signed in at 2016-10-22 19:09:23.'),
(47, 3, 'sign_in', '2016-10-22 23:21:11', 'User test signed in at 2016-10-22 19:21:11.'),
(48, 1, 'sign_in', '2016-10-22 23:22:34', 'User dev signed in at 2016-10-22 19:22:34.'),
(49, 1, 'sign_in', '2016-10-23 03:02:31', 'User dev signed in at 2016-10-22 23:02:31.'),
(50, 1, 'sign_in', '2016-10-23 03:03:01', 'User dev signed in at 2016-10-22 23:03:01.'),
(51, 1, 'sign_in', '2016-10-24 00:39:20', 'User dev signed in at 2016-10-23 20:39:20.'),
(52, 1, 'sign_in', '2016-10-24 09:11:17', 'User dev signed in at 2016-10-24 05:11:17.'),
(53, 1, 'sign_in', '2016-10-25 04:47:42', 'User dev signed in at 2016-10-25 00:47:42.'),
(54, 1, 'sign_in', '2016-10-26 00:22:37', 'User dev signed in at 2016-10-25 20:22:37.'),
(55, 1, 'sign_in', '2016-10-26 04:12:52', 'User dev signed in at 2016-10-26 00:12:52.'),
(56, 1, 'sign_in', '2016-10-26 05:45:44', 'User dev signed in at 2016-10-26 01:45:44.'),
(57, 1, 'sign_in', '2016-10-26 10:42:38', 'User dev signed in at 2016-10-26 06:42:38.'),
(58, 1, 'sign_in', '2016-10-27 11:16:01', 'User dev signed in at 2016-10-27 07:16:01.'),
(59, 1, 'sign_in', '2016-10-27 11:20:12', 'User dev signed in at 2016-10-27 07:20:12.'),
(60, 1, 'sign_in', '2016-10-27 23:55:36', 'User dev signed in at 2016-10-27 19:55:36.'),
(61, 1, 'sign_in', '2016-10-28 23:58:43', 'User dev signed in at 2016-10-28 19:58:43.'),
(62, 1, 'sign_in', '2016-10-30 04:37:24', 'User dev signed in at 2016-10-30 00:37:24.'),
(63, 4, 'sign_in', '2016-10-30 04:38:38', 'User Student signed in at 2016-10-30 00:38:38.'),
(64, 3, 'sign_in', '2016-10-30 04:40:05', 'User test signed in at 2016-10-30 00:40:05.'),
(65, 4, 'sign_in', '2016-10-30 04:40:39', 'User Student signed in at 2016-10-30 00:40:39.'),
(66, 3, 'sign_in', '2016-10-30 04:41:46', 'User test signed in at 2016-10-30 00:41:46.'),
(67, 1, 'sign_in', '2016-10-30 06:05:00', 'User dev signed in at 2016-10-30 02:05:00.'),
(68, 1, 'sign_in', '2016-10-31 07:18:11', 'User dev signed in at 2016-10-31 03:18:11.'),
(69, 1, 'sign_in', '2016-10-31 07:20:38', 'User dev signed in at 2016-10-31 03:20:38.'),
(70, 4, 'sign_in', '2016-10-31 07:21:22', 'User Student signed in at 2016-10-31 03:21:22.'),
(71, 3, 'sign_in', '2016-10-31 07:21:58', 'User test signed in at 2016-10-31 03:21:58.'),
(72, 1, 'sign_in', '2016-10-31 07:39:26', 'User dev signed in at 2016-10-31 03:39:26.'),
(73, 1, 'sign_in', '2016-10-31 11:53:33', 'User dev signed in at 2016-10-31 07:53:33.'),
(74, 1, 'sign_in', '2016-11-01 06:39:42', 'User dev signed in at 2016-11-01 02:39:42.'),
(75, 1, 'sign_in', '2016-11-01 08:13:45', 'User dev signed in at 2016-11-01 04:13:45.'),
(76, 4, 'sign_in', '2016-11-04 23:20:41', 'User Student signed in at 2016-11-04 19:20:41.'),
(77, 1, 'sign_in', '2016-11-04 23:21:05', 'User dev signed in at 2016-11-04 19:21:05.'),
(78, 3, 'sign_in', '2016-11-04 23:34:47', 'User test signed in at 2016-11-04 19:34:46.'),
(79, 1, 'sign_in', '2016-11-04 23:37:02', 'User dev signed in at 2016-11-04 19:37:02.'),
(80, 1, 'sign_in', '2016-11-05 22:24:09', 'User dev signed in at 2016-11-05 18:24:09.'),
(81, 1, 'sign_in', '2016-11-05 23:31:35', 'User dev signed in at 2016-11-05 19:31:35.'),
(82, 1, 'sign_in', '2016-11-07 05:09:49', 'User dev signed in at 2016-11-07 00:09:49.'),
(83, 3, 'sign_in', '2016-11-09 04:48:49', 'User test signed in at 2016-11-08 23:48:49.'),
(84, 1, 'sign_in', '2016-11-09 05:26:40', 'User dev signed in at 2016-11-09 00:26:40.'),
(85, 1, 'sign_in', '2016-11-10 23:32:31', 'User dev signed in at 2016-11-10 18:32:30.'),
(86, 1, 'sign_in', '2016-11-14 22:16:14', 'User dev signed in at 2016-11-14 17:16:14.'),
(87, 1, 'sign_in', '2016-11-14 22:54:31', 'User dev signed in at 2016-11-14 17:54:31.'),
(88, 1, 'sign_in', '2016-11-15 01:45:28', 'User dev signed in at 2016-11-14 20:45:28.'),
(89, 3, 'sign_in', '2016-11-17 02:47:37', 'User test signed in at 2016-11-16 21:47:37.'),
(90, 4, 'sign_in', '2016-11-17 02:47:58', 'User Student signed in at 2016-11-16 21:47:57.'),
(91, 1, 'sign_in', '2016-11-17 02:52:59', 'User dev signed in at 2016-11-16 21:52:59.'),
(92, 1, 'sign_in', '2016-11-18 22:58:55', 'User dev signed in at 2016-11-18 17:58:54.'),
(93, 1, 'sign_in', '2016-11-19 03:28:40', 'User dev signed in at 2016-11-18 22:28:40.'),
(94, 1, 'sign_in', '2016-11-27 23:30:50', 'User dev signed in at 2016-11-27 18:30:50.'),
(95, 1, 'sign_in', '2016-11-28 08:11:37', 'User dev signed in at 2016-11-28 03:11:37.'),
(96, 1, 'sign_in', '2016-12-01 00:18:01', 'User dev signed in at 2016-11-30 19:18:01.'),
(97, 3, 'sign_in', '2016-12-01 01:55:07', 'User test signed in at 2016-11-30 20:55:07.'),
(98, 4, 'sign_in', '2016-12-01 01:55:36', 'User Student signed in at 2016-11-30 20:55:36.'),
(99, 1, 'sign_in', '2016-12-01 01:57:38', 'User dev signed in at 2016-11-30 20:57:38.'),
(100, 1, 'sign_in', '2016-12-09 03:25:50', 'User dev signed in at 2016-12-08 22:25:50.'),
(101, 1, 'sign_in', '2016-12-09 23:20:00', 'User dev signed in at 2016-12-09 18:20:00.'),
(102, 1, 'sign_in', '2016-12-20 00:48:47', 'User dev signed in at 2016-12-19 19:48:46.'),
(103, 4, 'sign_in', '2016-12-20 00:49:43', 'User Student signed in at 2016-12-19 19:49:43.'),
(104, 1, 'sign_in', '2016-12-20 01:27:36', 'User dev signed in at 2016-12-19 20:27:36.'),
(105, 1, 'sign_in', '2016-12-31 11:44:45', 'User dev signed in at 2016-12-31 06:44:45.'),
(106, 4, 'sign_in', '2017-01-14 23:34:18', 'User Student signed in at 2017-01-14 18:34:17.'),
(107, 1, 'sign_in', '2017-01-22 03:46:46', 'User dev signed in at 2017-01-21 22:46:46.'),
(108, 4, 'sign_in', '2017-01-22 03:58:13', 'User Student signed in at 2017-01-21 22:58:13.'),
(109, 1, 'sign_in', '2017-01-22 21:22:02', 'User dev signed in at 2017-01-22 16:22:01.'),
(110, 1, 'sign_in', '2017-03-04 23:01:48', 'User dev signed in at 2017-03-04 18:01:48.'),
(111, 4, 'sign_in', '2017-03-04 23:02:55', 'User Student signed in at 2017-03-04 18:02:55.'),
(112, 1, 'sign_in', '2017-03-04 23:10:35', 'User dev signed in at 2017-03-04 18:10:35.'),
(113, 1, 'sign_in', '2017-03-12 00:11:59', 'User dev signed in at 2017-03-11 19:11:59.'),
(114, 1, 'sign_in', '2017-03-14 18:52:54', 'User dev signed in at 2017-03-14 14:52:54.'),
(115, 4, 'sign_in', '2017-03-14 18:55:03', 'User Student signed in at 2017-03-14 14:55:03.'),
(116, 1, 'sign_in', '2017-03-23 23:27:58', 'User dev signed in at 2017-03-23 19:27:57.'),
(117, 1, 'sign_in', '2017-03-25 03:57:22', 'User dev signed in at 2017-03-24 23:57:22.'),
(118, 4, 'sign_in', '2017-03-25 06:04:33', 'User Student signed in at 2017-03-25 02:04:33.'),
(119, 1, 'sign_in', '2017-03-28 05:33:34', 'User dev signed in at 2017-03-28 01:33:33.'),
(120, 1, 'sign_in', '2017-04-30 00:31:44', 'User dev signed in at 2017-04-29 20:31:44.'),
(121, 4, 'sign_in', '2017-04-30 01:47:02', 'User Student signed in at 2017-04-29 21:47:02.'),
(122, 1, 'sign_in', '2017-09-19 01:34:39', 'User dev signed in at 2017-09-18 21:34:39.'),
(123, 4, 'sign_in', '2017-09-19 04:50:59', 'User Student signed in at 2017-09-19 00:50:59.'),
(124, 1, 'sign_in', '2017-09-19 04:51:31', 'User dev signed in at 2017-09-19 00:51:31.'),
(125, 3, 'sign_in', '2017-09-19 04:51:59', 'User test signed in at 2017-09-19 00:51:59.'),
(126, 1, 'sign_in', '2017-09-19 04:53:53', 'User dev signed in at 2017-09-19 00:53:53.'),
(127, 1, 'sign_in', '2017-09-22 01:20:50', 'User dev signed in at 2017-09-21 21:20:50.'),
(128, 1, 'sign_in', '2017-09-22 02:39:50', 'User dev signed in at 2017-09-21 22:39:50.'),
(129, 1, 'sign_in', '2017-09-22 06:25:38', 'User dev signed in at 2017-09-22 02:25:38.'),
(130, 1, 'sign_in', '2017-09-28 08:36:07', 'User dev signed in at 2017-09-28 04:36:07.'),
(131, 4, 'sign_in', '2017-09-28 08:46:06', 'User Student signed in at 2017-09-28 04:46:06.'),
(132, 1, 'sign_in', '2017-10-04 00:41:05', 'User dev signed in at 2017-10-03 20:41:05.'),
(133, 1, 'sign_in', '2017-10-05 01:47:53', 'User dev signed in at 2017-10-04 21:47:53.'),
(134, 1, 'sign_in', '2017-10-13 02:55:51', 'User dev signed in at 2017-10-12 22:55:51.'),
(135, 3, 'sign_in', '2017-10-13 03:21:13', 'User test signed in at 2017-10-12 23:21:13.'),
(136, 1, 'sign_in', '2017-10-13 04:38:37', 'User dev signed in at 2017-10-13 00:38:37.'),
(137, 1, 'sign_in', '2017-10-18 04:17:45', 'User dev signed in at 2017-10-18 00:17:45.'),
(138, 1, 'sign_in', '2017-10-18 23:07:05', 'User dev signed in at 2017-10-18 19:07:04.'),
(139, 1, 'sign_in', '2017-10-20 23:08:36', 'User dev signed in at 2017-10-20 19:08:36.');

-- --------------------------------------------------------

--
-- Table structure for table `uf_user_rememberme`
--

CREATE TABLE `uf_user_rememberme` (
  `user_id` int(11) NOT NULL,
  `token` varchar(40) NOT NULL,
  `persistent_token` varchar(40) NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `earth_events`
--
ALTER TABLE `earth_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `earth_events_user`
--
ALTER TABLE `earth_events_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`marker_id`),
  ADD KEY `user-relation` (`user_id`);

--
-- Indexes for table `uf_authorize_group`
--
ALTER TABLE `uf_authorize_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_authorize_user`
--
ALTER TABLE `uf_authorize_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_configuration`
--
ALTER TABLE `uf_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_group`
--
ALTER TABLE `uf_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_group_user`
--
ALTER TABLE `uf_group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_user`
--
ALTER TABLE `uf_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uf_user_event`
--
ALTER TABLE `uf_user_event`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `earth_events`
--
ALTER TABLE `earth_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `earth_events_user`
--
ALTER TABLE `earth_events_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `markers`
--
ALTER TABLE `markers`
  MODIFY `marker_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uf_authorize_group`
--
ALTER TABLE `uf_authorize_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `uf_authorize_user`
--
ALTER TABLE `uf_authorize_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uf_configuration`
--
ALTER TABLE `uf_configuration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `uf_group`
--
ALTER TABLE `uf_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `uf_group_user`
--
ALTER TABLE `uf_group_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `uf_user`
--
ALTER TABLE `uf_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `uf_user_event`
--
ALTER TABLE `uf_user_event`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `markers`
--
ALTER TABLE `markers`
  ADD CONSTRAINT `user-relation` FOREIGN KEY (`user_id`) REFERENCES `uf_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
