$(document).ready(function () {

    function getURLParams() {
        var parameters = location.search.substring(1).split("&");

        var temp = parameters[0].split("=");
        l = unescape(temp[1]);

        temp = parameters[1].split("=");
        c = unescape(temp[1]);

        function getMapStuff(parameters) {
            var mapProp = new Object();
            var temp = parameters[0].split("=");
            mapProp['lat'] = unescape(temp[1]);
            temp = parameters[1].split("=");
            mapProp['lng'] = unescape(temp[1]);
            temp = parameters[2].split("=");
            mapProp['before'] = unescape(temp[1]);
            temp = parameters[3].split("=");
            mapProp['after'] = unescape(temp[1]);
            temp = parameters[5].split("=");
            mapProp['location'] = unescape(temp[1]);
            return mapProp;
        }
        var mapData = getMapStuff(parameters);

        //document.getElementById("results").innerHTML = l;
        return mapData;

    }



    var uriData = getURLParams();
    var aoiTitle = uriData.location;
    var before = uriData.before;
    var after = uriData.after;

    function setTitle(name, title) {
        var s = document.getElementById(name);
        s.innerHTML = title;
    }

// http://localhost/ae/map/layers?lat=-81.78359985351564&lng=26.14218603473982&from=2017-07-31T14:00:00.000Z&until=2017-10-04T00:00:00.000Z&id=69&name=Naples,%20Florida
    function setToolLink(layerLink) {
        var s = document.getElementById(layerLink).setAttribute("href", site.uri.public + "/map/layers/?lat=" + uriData.lat + "&lng=" + uriData.lng + "&from=" + uriData.before+ "&until=" + uriData.after+ "&id=" + uriData.id+ "&location=" + aoiTitle);
    }


    window.onload = setTitle('eventName', aoiTitle);
    window.onload = setToolLink("layerLink");

    var map = L.map('map').setView([uriData.lng, uriData.lat
    ], 12);

    L.control.fullscreen().addTo(map);

    L.control.scale().addTo(map);

    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

    var osm2 = new L.TileLayer(osmUrl, { minZoom: 0, maxZoom: 13 });
    var miniMap = new L.Control.MiniMap(osm2, { toggleDisplay: true, position: 'topright' }).addTo(map);

    var measureControl = new L.Control.Measure({ position: 'topleft', primaryLengthUnit: 'meters', secondaryLengthUnit: 'kilometres', primaryAreaUnit: 'sqmeters', secondaryAreaUnit: 'hectares', activeColor: '#71c8f1', completedColor: '#02ab4d' });


    var north = L.control({ position: "bottomright" });
    north.onAdd = function (map) {
        var div = L.DomUtil.create("div", "info legend");
        div.innerHTML = '<img src="../images/north.png">';
        return div;
    }
    north.addTo(map);

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        }
    });
    map.addControl(drawControl);

    map.on('draw:created', function (e) {
        var type = e.layerType,
            layer = e.layer;

        if (type === 'marker') {
            layer.bindPopup('A popup!');
        }

        drawnItems.addLayer(layer);
    });

    map.on('draw:edited', function (e) {
        var layers = e.layers;
        var countOfEditedLayers = 0;
        layers.eachLayer(function (layer) {
            countOfEditedLayers++;
        });
        console.log("Edited " + countOfEditedLayers + " layers");
    });



    //Swipe Functions

    var urlLayer12 = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=4045B62071B54A128DC2&api_secret=BCD155EE5D58473782297B7E30AEF695&cloud_coverage_lte=10&acquired_lte=` + after;
    var urlLayer13 = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=4045B62071B54A128DC2&api_secret=BCD155EE5D58473782297B7E30AEF695&cloud_coverage_lte=10&acquired_lte=` + before;
    var urlLayer10 = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=4045B62071B54A128DC2&api_secret=BCD155EE5D58473782297B7E30AEF695&cloud_coverage_lte=10&&acquired_lte=` + before
    var urlLayer11 = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=4045B62071B54A128DC2&api_secret=BCD155EE5D58473782297B7E30AEF695&cloud_coverage_lte=10&&acquired_lte=` + before
    var url = 'https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=4045B62071B54A128DC2&api_secret=BCD155EE5D58473782297B7E30AEF695&cloud_coverage_lte=20&acquired_gte=2017-01-23T13:00:00.000Z&acquired_lte=2017-03-25T12:59:59.999Z';

    var urlLayer1 = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=F579D945208F4F1C8F01&api_secret=3AAC25FD7F6945B1B3BA7DB6E8E453A3&sensor_platform=landsat-8,theia&acquired_gte=2015-11-30T13:00:00.000Z&acquired_lte=2016-04-01T12:59:59.999Z`;

    var urlLayer2 = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=F579D945208F4F1C8F01&api_secret=3AAC25FD7F6945B1B3BA7DB6E8E453A3sensor_platform=landsat-8,theia&acquired_gte=2015-11-30T13:00:00.000Z&acquired_lte=2015-12-31T12:59:59.999Z`;

    var urlLayer3 = `https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;

    var urlLayer4 = `https://api.mapbox.com/styles/v1/mapbox/terrain-v2/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;
    //var bUrl = 'https://api.tiles.mapbox.com/v4/surface/mapbox.landsat-live/256/{z}/{x}/{y}?layer=scene&fields=date,description,processed_date,scene_id,source_id&access_token=pk.eyJ1IjoiZG5vbWFkYiIsImEiOiJjaW16ZngwOGkwNGtvdm9sdWp4dnc2cWxvIn0.G2DN19xoW2FkzKb5YxtXxA&points=' + latlng.lng%180 + ','+ latlng.lat;

    var ucTiles1 = L.tileLayer(urlLayer13, {
        minZoom: 0,
        maxZoom: 14,
        maxNativeZoom: 13,
    }).addTo(map);

    var ucTiles2 = L.tileLayer(urlLayer12, {
        minZoom: 0,
        maxZoom: 14,
        maxNativeZoom: 13,
    }).addTo(map);

    var mbTiles = L.tileLayer(urlLayer3, {
        minZoom: 15,
        maxZoom: 18,
        maxNativeZoom: 17,
    }).addTo(map);

    var range = document.getElementById('range');

    function clip() {
        var nw = map.containerPointToLayerPoint([0, 0]),
            se = map.containerPointToLayerPoint(map.getSize()),
            clipX = nw.x + (se.x - nw.x) * range.value;

        ucTiles2.getContainer().style.clip = 'rect(' + [nw.y, clipX, se.y, nw.x].join('px,') + 'px)';
    }

    range['oninput' in range ? 'oninput' : 'onchange'] = clip;

    map.on('move', clip);

    clip();

    var geocoder = L.Control.geocoder({
        defaultMarkGeocode: false
    })
        .on('markgeocode', function (e) {
            var bbox = e.geocode.bbox;
            var poly = L.polygon([
                bbox.getSouthEast(),
                bbox.getNorthEast(),
                bbox.getNorthWest(),
                bbox.getSouthWest()
            ]).addTo(map);
            map.fitBounds(poly.getBounds());
        })
        .addTo(map);
    /*
      L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
       minZoom: 0,
      maxZoom: 14,
      maxNativeZoom: 13,
      }).addTo(map);
    
      L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      minZoom: 15,   
       maxZoom: 18,
       maxNativeZoom: 17,
      }).addTo(map);
    */

    //addLayer(L.tileLayer(urlLayer4).addTo(map), 'Terrain?', 1);

    function addLayer(layer, name, zIndex) {
        layer
            .setZIndex(zIndex)
            .addTo(map);

        // Create a simple layer switcher that
        // toggles layers on and off.
        var link = document.createElement('a');
        link.href = '#';
        link.className = 'active';
        link.innerHTML = name;

        link.onclick = function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (map.hasLayer(layer)) {
                map.removeLayer(layer);
                this.className = '';
            } else {
                map.addLayer(layer);
                this.className = 'active';
            }
        };

        var layers = document.getElementById('menu-ui');

        layers.appendChild(link);
    }

    measureControl.addTo(map);
    //console.log(this.markers);
    //var userMarker = L.marker([51.5, -0.09]).addTo(mymap);
 //gets table
    var oTable = document.getElementById('markers');

    //gets rows of table
    var rowLength = oTable.rows.length;

    //loops through rows    
    for (i = 1; i < rowLength; i++){

      //gets cells of current row  
       var oCells = oTable.rows.item(i).cells;

       //gets amount of cells of current row
       var cellLength = oCells.length;

       //loops through each cell in current row
       for(var j = 0; j < cellLength - 1; j++){

              var desc = oCells.item(0).innerHTML;
              var lat = oCells.item(1).innerHTML;
              var lng = oCells.item(2).innerHTML;
                var userMarker = L.marker([lat, lng]).addTo(map);
            L.marker([lng, lat], {/*icon: customicon*/}) // <== Coordinates order in Leaflet is [lat, lng]
                .addTo(map)
                .bindPopup("xfcgs");
                
              console.log(desc, lat, lng);
              console.log(userMarker);
           }

       
              // get your cell info here

    }


    

});
 