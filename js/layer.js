$(document).ready(function () {

    function getURLParams() {
        var parameters = location.search.substring(1).split("&");

        var temp = parameters[0].split("=");
        l = unescape(temp[1]);

        temp = parameters[1].split("=");
        c = unescape(temp[1]);

        function getMapStuff(parameters) {
            var mapProp = new Object();
            var temp = parameters[0].split("=");
            mapProp['lat'] = unescape(temp[1]);
            temp = parameters[1].split("=");
            mapProp['lng'] = unescape(temp[1]);
            temp = parameters[2].split("=");
            mapProp['before'] = unescape(temp[1]);
            temp = parameters[3].split("=");
            mapProp['after'] = unescape(temp[1]);
            temp = parameters[5].split("=");
            mapProp['location'] = unescape(temp[1]);
            return mapProp;
        }
        var mapData = getMapStuff(parameters);

        //document.getElementById("results").innerHTML = l;
        return mapData;

    }



    //var example = getURLParams();
    //console.log(example.lng);

    var uriData = getURLParams();

    var aoiTitle = uriData.location;
    var before = uriData.before;
    var after = uriData.after;

    // console.log(areaLng);


    var map = L.map('map').setView([uriData.lng, uriData.lat
    ], 12);

    L.control.fullscreen().addTo(map);


    L.control.scale().addTo(map);

    function setTitle(name, title) {
        var s = document.getElementById(name);
        s.innerHTML = title;
    }

    // http://localhost/ae/map/layers?lat=-81.78359985351564&lng=26.14218603473982&from=2017-07-31T14:00:00.000Z&until=2017-10-04T00:00:00.000Z&id=69&name=Naples,%20Florida
    function setToolLink(layerLink) {
        var s = document.getElementById(layerLink).setAttribute("href", site.uri.public + "/map/swipe/?lat=" + uriData.lat + "&lng=" + uriData.lng + "&from=" + uriData.before + "&until=" + uriData.after + "&id=" + uriData.id + "&location=" + aoiTitle);
    }


    window.onload = setTitle('eventName', aoiTitle);
    window.onload = setToolLink("swipeLink");


    var measureControl = new L.Control.Measure({ position: 'topleft', primaryLengthUnit: 'meters', secondaryLengthUnit: 'kilometres', primaryAreaUnit: 'sqmeters', secondaryAreaUnit: 'hectares', activeColor: '#71c8f1', completedColor: '#02ab4d' });
    measureControl.addTo(map);

    var north = L.control({ position: "bottomright" });
    north.onAdd = function (map) {
        var div = L.DomUtil.create("div", "info legend");
        div.innerHTML = '<img src="../images/north.png">';
        return div;
    }
    north.addTo(map);

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    // Initialise the draw control and pass it the FeatureGroup of editable layers
    var drawControl = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        }
    });
    map.addControl(drawControl);

    let oTable = document.getElementById('markers');
    let group = L.layerGroup()
    let list = document.getElementById('list');
    let featureTable = document.getElementById('ftable');
    let layerSwitch = document.getElementById('markerSwitch');
    let rowLength = oTable.rows.length;
    let markers = [];
    map.on('draw:created', function (e) {
        var type = e.layerType,
            layer = e.layer;

        console.log(e, layer);
        // e.layer.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
        var d = new Date();
        if (type === "marker") {
// var popupContent = '<form role="form"  action="{{form_action}}" id="form" enctype="multipart/form-data" class = "form-horizontal" >'+
//           '<div class="form-group">'+
//               '<label class="control-label col-sm-5"><strong>Date: </strong></label>'+
//               '<input type="date" placeholder="Required" id="date" name="date" class="form-control"/>'+ 
//           '</div>'+
//           '<div class="form-group">'+
//               '<label class="control-label col-sm-5"><strong>Gender: </strong></label>'+
//               '<select class="form-control" id="gender" name="gender">'+
//                 '<option value="Male">Male</option>'+
//                 '<option value="Female">Female</option>'+
//                 '<option value="Other">Other</option>'+
//               '</select>'+ 
//           '</div>'+
//           '<div class="form-group">'+
//               '<label class="control-label col-sm-5"><strong>Age: </strong></label>'+
//               '<input type="number" min="0" class="form-control" id="age" name="age">'+ 
//           '</div>'+
//           //...
//           '<div class="form-group">'+
//               '<label class="control-label col-sm-5"><strong>Description: </strong></label>'+
//               '<textarea class="form-control" rows="6" id="descrip" name="descript">...</textarea>'+
//           '</div>'+
//           '<input style="display: none;" type="text" id="lat" name="lat" value="" />'+
//           '<input style="display: none;" type="text" id="lng" name="lng" value="" />'+
//           '<div class="form-group">'+
//             '<div style="text-align:center;" class="col-xs-4 col-xs-offset-2"><button type="button" class="btn">Cancel</button></div>'+
//             '<div style="text-align:center;" class="col-xs-4"><button type="submit" value="submit" class="btn btn-primary trigger-submit">Submit</button></div>'+
//           '</div>'+
//           '</form>';
var popupContent = "<b>Feature Details</b><br>Type: " +
                e.layerType + "<br>" +
                "LatLng: " + layer.getLatLng() + "<br>" +
                "Created: " + d.toDateString();
            drawnItems.addLayer(layer).bindPopup(popupContent,{
            keepInView: true,
            closeButton: false
            }).openPopup();


            

            var desc = prompt("Enter a description", "");

            if (desc == null || desc == "") {

                txt = "User cancelled the prompt.";

            } else {

                let tmp = new Object();
                tmp.id = layer._leaflet_id;
                tmp.desc = desc;
                tmp.latLng = layer.getLatLng();
                tmp.marker_id = drawnItems.getLayerId(layer);
                let currentUserID = document.getElementById("cUser").value;
                //console.log(tmp.latLng.lat);

                
                let row = featureTable.insertRow(1);
                let id = row.insertCell(0);
                let tableDesc = row.insertCell(1);
                let long = row.insertCell(2);
                let lat = row.insertCell(3);

                id.innerHTML =`<a href="#">`+ tmp.id +`</a>`;
                tableDesc.innerHTML = tmp.desc;
                long.innerHTML = tmp.latLng.lng;
                lat.innerHTML = tmp.latLng.lat;

                markers.push(tmp);


                row.addEventListener('click', onClick.bind(null, tmp));
                var csrf_token = $("meta[name=csrf_token]").attr("content");
                //serializedData += "&csrf_token=" + encodeURIComponent(csrf_token); 
                console.log(csrf_token);
                $.ajax({
                    type: "POST",
                    url: 'test/?',
                    data: {
                        user_id: currentUserID,
                        description: tmp.desc,
                        latitude: tmp.latLng.lat,
                        longitude: tmp.latLng.lng,
                        marker_id: tmp.marker_id,
                        csrf_token: csrf_token
                    },
                    success: function(data){
                        console.log(this.data);
                    }
                });


                console.log(markers);
            }

            
        }
            else {

                 drawnItems.addLayer(layer).bindPopup(
                "<b>Feature Details</b><br>Type: " +
                e.layerType + "<br>" +
                "Created: " + d.toDateString());
            }

    });

    map.on('draw:edited', function (e) {
        var layers = e.layers;
        var countOfEditedLayers = 0;
        layers.eachLayer(function (layer) {
            countOfEditedLayers++;
        });
        console.log("Edited " + countOfEditedLayers + " layers");
    });


    // Confirm we've got 'em by displaying them to the screen
    var apiKey = 'BB70FD54EBB7483882BC',
        apiSecret = 'A69E7AE6C49C455C856386A130DF299D';


    var rgbLayerBefore = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + before,
        ndviLayerBefore = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + before,
        ndwiLayerBefore = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + before,
        fcnirLayerBefore = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + before,
        eviLayerBefore = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + before;

    var rgbLayerAfter = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + after,
        ndviLayerAfter = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + after,
        ndwiLayerAfter = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + after,
        fcnirLayerAfter = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + after,
        eviLayerAfter = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=` + after;

    var urlLayer3 = `https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;


    // Append them to the map
     addLayer(L.tileLayer(rgbLayerBefore).addTo(map), 'RGB - Before', 1);
     addLayer(L.tileLayer(ndviLayerBefore).addTo(map), 'NDVI - Before', 2);
     addLayer(L.tileLayer(ndwiLayerBefore).addTo(map), 'NDWI - Before', 3);
     addLayer(L.tileLayer(fcnirLayerBefore).addTo(map), 'False Color NIR - Before', 4);
     addLayer(L.tileLayer(eviLayerBefore).addTo(map), 'EVI - Before', 5);
     addLayer(L.tileLayer(rgbLayerAfter).addTo(map), 'RGB - After', 1);
     addLayer(L.tileLayer(ndviLayerAfter).addTo(map), 'NDVI - After', 2);
     addLayer(L.tileLayer(ndwiLayerAfter).addTo(map), 'NDWI - After', 3);
     addLayer(L.tileLayer(fcnirLayerAfter).addTo(map), 'False Color NIR - After', 4);
     addLayer(L.tileLayer(eviLayerAfter).addTo(map), 'EVI - After', 5);

    var mbTiles = L.tileLayer(urlLayer3, {
        minZoom: 15,
        maxZoom: 18,
        maxNativeZoom: 17,
    }).addTo(map);

    // L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //     minZoom: 0,
    //     maxZoom: 14,
    //     maxNativeZoom: 13,
    // }).addTo(map);

    function addLayer(layer, name, zIndex) {
        layer
            .setZIndex(zIndex)
            .addTo(map);

        // Create a simple layer switcher that
        // toggles layers on and off.
        var link = document.createElement('a');
        link.href = '#';
        link.className = 'active';
        link.innerHTML = name;

        link.onclick = function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (map.hasLayer(layer)) {
                map.removeLayer(layer);
                this.className = '';
            } else {
                map.addLayer(layer);
                this.className = 'active';
            }
        };

        var layers = document.getElementById('map-ui');

        layers.appendChild(link);
    }

    for (i = 1; i < rowLength; i++) {


        //gets cells of current row  
        var oCells = oTable.rows.item(i).cells;

        //gets amount of cells of current row
        var cellLength = oCells.length;

        var id = oCells.item(0).innerHTML;
        var desc = oCells.item(1).innerHTML;
        var lng = oCells.item(2).innerHTML;
        var lat = oCells.item(3).innerHTML;

        var tmp = new Object();
        tmp.id = id;
        tmp.desc = desc;
        tmp.latLng = [lng, lat];

        markers.push(tmp);

        console.log(markers);



    }
    // Loop through the data
    markers.forEach(person => {
        console.log(person.latLng);
        let marker = L.marker(person.latLng, {
            title: person.name,
            riseOnHover: true
        });

        // Add each marker to the group
        drawnItems.addLayer(marker).bindPopup(person.desc);


        // Save the ID of the marker with it's data
        person.marker_id = drawnItems.getLayerId(marker);
    })

    // Click handler for handling
    function onClick(data) {
        let { marker_id } = data,
            marker = drawnItems.getLayer(marker_id);
        console.log(data);

        //map.panTo(marker.latLng[0].toFixed(6), marker.latLng[0].toFixed(6) );
        map.panTo(marker.getLatLng());
        marker.openPopup();
    }



    // Append list items
    markers.forEach(person => {
        let item = document.createElement('tr');

        item.innerHTML = `<td><a href="#">${person.id}</a></td>` +
            `<td><a href="#">${person.desc}</a></td>` +
            `<td><a href="#">${person.latLng[1]}</a></td>` +
            `<td><a href="#">${person.latLng[0]}</a></td>` +
            `<td>
                <label class="btn btn-secondary">
                    <input id="markerSwitch" type="checkbox" autocomplete="off">
                     Display
                </label>
            </td>`;

        item.addEventListener('click', onClick.bind(null, person));

        featureTable.appendChild(item);
    });


});