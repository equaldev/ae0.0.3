$.get('https://watchers.news/category/editors-picks/feed/', function (data) {
    $(data).find("item").each(function () { // or "item" or whatever suits your feed
        var el = $(this);

        console.log("------------------------");
        console.log("title      : " + el.find("title").text());
        console.log("author     : " + el.find("pubDate").text());
        console.log("description: " + el.find("description").text());
    });
});