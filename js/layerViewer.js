
// Confirm we've got 'em by displaying them to the screen
var apiKey = 'BB70FD54EBB7483882BC',
    apiSecret = 'A69E7AE6C49C455C856386A130DF299D';

// Create a Leaflet map
var map = L.mapbox.map('map').setView([
    -34.598611, 150.867778
], 12);
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);

// Initialise the draw control and pass it the FeatureGroup of editable layers
var drawControl = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    }
});
map.addControl(drawControl);

        map.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer;

            if (type === 'marker') {
                layer.bindPopup('A popup!');
            }

            drawnItems.addLayer(layer);
        });

        map.on('draw:edited', function (e) {
            var layers = e.layers;
            var countOfEditedLayers = 0;
            layers.eachLayer(function(layer) {
                countOfEditedLayers++;
            });
            console.log("Edited " + countOfEditedLayers + " layers");
        });

L.control.fullscreen().addTo(map);

var geocoder = L.Control.geocoder({
        defaultMarkGeocode: false
    })
    .on('markgeocode', function(e) {
        var bbox = e.geocode.bbox;
        var poly = L.polygon([
             bbox.getSouthEast(),
             bbox.getNorthEast(),
             bbox.getNorthWest(),
             bbox.getSouthWest()
        ]).addTo(map);
        map.fitBounds(poly.getBounds());
    })
    .addTo(map);
var rgbLayerBefore = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2015-07-31T14:00:00.000Z&acquired_lte=2015-08-21T13:59:59.999Z`,
    ndviLayerBefore = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2015-07-31T14:00:00.000Z&acquired_lte=2015-08-21T13:59:59.999Z`,
    ndwiLayerBefore = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2015-07-31T14:00:00.000Z&acquired_lte=2015-08-21T13:59:59.999Z`,
    fcnirLayerBefore= `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2015-07-31T14:00:00.000Z&acquired_lte=2015-08-21T13:59:59.999Z`,
    eviLayerBefore = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2015-07-31T14:00:00.000Z&acquired_lte=2015-08-21T13:59:59.999Z`;

var rgbLayerAfter = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2016-07-31T14:00:00.000Z&acquired_lte=2016-08-21T13:59:59.999Z`,
    ndviLayerAfter = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2016-07-31T14:00:00.000Z&acquired_lte=2016-08-21T13:59:59.999Z`,
    ndwiLayerAfter = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2016-07-31T14:00:00.000Z&acquired_lte=2016-08-21T13:59:59.999Z`,
    fcnirLayerAfter = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2016-07-31T14:00:00.000Z&acquired_lte=2016-08-21T13:59:59.999Z`,
    eviLayerAfter = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=30&acquired_gte=2016-07-31T14:00:00.000Z&acquired_lte=2016-08-21T13:59:59.999Z`;
    
// Append them to the map
addLayer(L.tileLayer(rgbLayerBefore).addTo(map), 'RGB - Before', 1);
addLayer(L.tileLayer(ndviLayerBefore).addTo(map), 'NDVI - Before', 2);
addLayer(L.tileLayer(ndwiLayerBefore).addTo(map), 'NDWI - Before', 3);
addLayer(L.tileLayer(fcnirLayerBefore).addTo(map), 'False Color NIR - Before', 4);
addLayer(L.tileLayer(eviLayerBefore).addTo(map), 'EVI - Before', 5);
addLayer(L.tileLayer(rgbLayerAfter).addTo(map), 'RGB - After', 1);
addLayer(L.tileLayer(ndviLayerAfter).addTo(map), 'NDVI - After', 2);
addLayer(L.tileLayer(ndwiLayerAfter).addTo(map), 'NDWI - After', 3);
addLayer(L.tileLayer(fcnirLayerAfter).addTo(map), 'False Color NIR - After', 4);
addLayer(L.tileLayer(eviLayerAfter).addTo(map), 'EVI - After', 5);
// From https://www.mapbox.com/mapbox.js/example/v1.0.0/layers/
function addLayer(layer, name, zIndex) {
    layer
        .setZIndex(zIndex)
        .addTo(map);

    // Create a simple layer switcher that
    // toggles layers on and off.
    var link = document.createElement('a');
        link.href = '#';
        link.className = 'active';
        link.innerHTML = name;

    link.onclick = function(e) {
        e.preventDefault();
        e.stopPropagation();

        if (map.hasLayer(layer)) {
            map.removeLayer(layer);
            this.className = '';
        } else {
            map.addLayer(layer);
            this.className = 'active';
        }
    };
    
    var layers = document.getElementById('map-ui');

    layers.appendChild(link);
}

