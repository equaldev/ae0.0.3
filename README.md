# README #

# MODELLING IN PROGRESS FOR TRANSFER TO JAVA/ANGULAR application. #

heres an awesome link for accessing multispectral landsat 7 & 8 imagery and converting it into a tileset:
https://www.mapbox.com/help/processing-satellite-imagery/#next-steps


Heres another that illustrates processing of raw landsat bands into a tile layer hosted by MapBox.
https://tilemill-project.github.io/tilemill/docs/guides/landsat-8-imagery/

# landsat-util #

The landsat-util can be forked from github and compiled from source unless your OS offers it in a binary ready to go.

The blog describes it simply as:

a command line utility that makes it easy to search, download, and process Landsat imagery.
You can search based on date, cloud coverage % and other things, download immediately, or process once it's downloaded like pansharpen or stitch the images together.

You can preview images before you download. Search commands provide a link to a thumbnail for each image.

landsat search  --cloud 4 --start "August 1 2013" --end "August 25 2014" country 'Vatican'

Using the --pansharpen flag will take longer to process but will produce clearer images.

landsat search --download --imageprocess --pansharpen  --cloud 4 --start "august 11 2013" --end "august 13 2013" pr 191 031

You can also perform all processing on images that you previously downloaded.

landsat download LC81050682014217LGN00

landsat process --pansharpen /your/path/LC81050682014217LGN00.tar.bz


### What is this repository for? ###

* A tool to visualise Earth change over time.
* Version 0.0.3
* Will fill this out properly when I get a bit of time.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact